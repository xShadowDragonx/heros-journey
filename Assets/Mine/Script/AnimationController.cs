﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
    [SerializeField]
        float jumpForce;
    Animator anim;
    Rigidbody rb;
    bool land, attack;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {


        anim.SetBool("Land", land);
        anim.SetBool("Attack", attack);


		if (Input.GetKeyDown(KeyCode.G) &&attack == false) {
            anim.SetTrigger("Kick");
            attack = true;
        }
            if (Input.GetKey(KeyCode.H) &&attack == false) {  
                    anim.SetTrigger("Punch");
            attack = true;
}
        if(Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f) {
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }
        if (land)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetTrigger("Jump");
            }
        }
    }   
    private void OnCollisionStay(Collision coll)
    {
     if(coll.gameObject.tag == "Land")
        {
            land = true;
        }
        else        
        {
            land = false;
        }
     // se puede resumir en:  land = (coll.gameObject.tag == "Land") ? true : false
    }
    private void OnCollisionExit(Collision drt)
    {
        if (drt.gameObject.tag == "Land")
        {
            land = false;
        }
    }
    void ActualJump()
    {
        print("jumper");
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}


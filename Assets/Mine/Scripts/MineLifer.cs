﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AkamaLife
{
    public class MineLifer : MonoBehaviour
    {
        public float currentLife;
        public bool alive;
        [SerializeField] float initialLife;
        public float maxLife;
        [SerializeField]NewMineTird mineTird;
        [SerializeField]Enemie1 enemie;
        private void Start()
        {
            mineTird = GetComponent<NewMineTird>();
            enemie = GetComponent<Enemie1>();
            currentLife = initialLife;
        }
        private void Update()
        {
            alive = (currentLife > 0) ? true : false;
        }
        public void Damage(float amount)
        {
            if (currentLife - amount > 0)
            {
                if (mineTird != null)
                {
                    mineTird.AnimateThis("Damage");
                    mineTird.AbleToAttack();
                }
                if (enemie != null)
                {
                    enemie.AnimateThis("Damage");
                    enemie.AbleToAttack();
                }
            }
            currentLife -= amount;
        }
        public void Heal(float amount)
        {
            if (currentLife >= maxLife)
                return;
            else
                currentLife += amount;
            if (mineTird != null)
                mineTird.AnimateThis("Heal");
        }
    }
}